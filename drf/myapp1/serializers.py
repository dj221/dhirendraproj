from rest_framework import serializers
from .models import *


class StudentInfoSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    firstname = serializers.CharField(max_length=20)
    lastname = serializers.CharField(max_length=20)
    age = serializers.IntegerField()
    address = serializers.CharField(max_length=50)

    def create(self, validated_data):
        return StudentInfo.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.firstname = validated_data.get('firstname',instance.firstname)
        instance.lastname = validated_data.get('lastname', instance.lastname)
        instance.age = validated_data.get('age', instance.age)
        instance.address = validated_data.get('address', instance.address)
        instance.save()
        return instance