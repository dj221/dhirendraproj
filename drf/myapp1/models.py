from django.db import models


class StudentInfo(models.Model):
    firstname = models.CharField(max_length=20,null=False)
    lastname = models.CharField(max_length=20,null=False)
    age = models.IntegerField()
    address = models.CharField(max_length=50)

    def __str__(self):
        return str(self.firstname) + " " + str(self.lastname)


PROJECT_SIZE = (
    ('big','BIG'),
    ('moderate','MODERATE'),
    ('small','SMALL'),
)

class Project(models.Model):
    studentsname = models.ForeignKey(StudentInfo, on_delete=models.CASCADE)
    projectname = models.CharField(max_length=30)
    projectcost = models.FloatField()
    projectsize = models.CharField(max_length=10,choices=PROJECT_SIZE)

    def __str__(self):
        return str(self.studentsname) + "->" + str(self.projectname)

