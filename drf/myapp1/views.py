from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import StudentInfo
from .serializers import StudentInfoSerializer


@api_view(['GET','POST'])
def student_list(request,format=None):
    if request.method == 'GET':
        studentsinfo = StudentInfo.objects.all()
        serializer = StudentInfoSerializer(studentsinfo,many=True)
        return Response(serializer.data)
    elif request.method == 'POST':
        serializer = StudentInfoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET','PUT','DELETE'])
def student_detail(request, pk, format=None):
    try:
        students = StudentInfo.objects.get(pk=pk)
    except StudentInfo.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = StudentInfoSerializer(students)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = StudentInfoSerializer(students, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        students.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


